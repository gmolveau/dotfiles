export ZSH=${HOME}/.oh-my-zsh
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="robbyrussell"
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
plugins=(
  git
  python
  zsh-autosuggestions # github.com/zsh-users/zsh-autosuggestions
  zsh-syntax-highlighting # github.com/zsh-users/zsh-syntax-highlighting.git
)
source ${ZSH}/oh-my-zsh.sh
test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"
autoload -Uz compinit && compinit -i -d ${HOME}/.cache/zsh/zcompdump-${ZSH_VERSION}

#-- EXPORTS --#

export EDITOR='sublime -w'
export LANG='en_US.UTF-8'
export LC_ALL='en_US.UTF-8'
# Don’t clear the screen after quitting a manual page.
export MANPAGER='less -X'
export XDG_DATA_HOME=${HOME}/.local/share
export XDG_CONFIG_HOME=${HOME}/.config
export XDG_CACHE_HOME=${HOME}/.cache
# Make Python use UTF-8 encoding for output to stdin, stdout, and stderr.
export PYTHONIOENCODING='UTF-8'
# Binaries
export USER_BIN=${HOME}/bin
export GOPATH=${HOME}/go
export GOBIN=${GOPATH}/bin
export CARGO_BIN=${HOME}/.cargo/bin
export PIP_BIN=${HOME}/Library/Python/3.7/bin
export NODE_BIN=/usr/local/opt/node@10/bin

export PATH=${PATH}:/usr/local/bin:/usr/local/sbin:${USER_BIN}:${GOBIN}:${CARGO_BIN}:${PIP_BIN}:${NODE_BIN}

#-- ALIASES --#

alias dotfiles='/usr/bin/git --git-dir=${HOME}/.dotfiles/ --work-tree=${HOME}'
alias ll="ls -alh"
alias myip="dig +short myip.opendns.com @resolver1.opendns.com"
alias delete_ds_store="find . -name '.DS_Store' -type f -delete"
alias shrug='echo -E "¯\_(ツ)_/¯" | tee /dev/tty | pbcopy'
alias reload="exec ${SHELL} -l"
alias path='echo -e ${PATH//:/\\n}'

#-- FUNCTIONS --#

# cd to the parent directory of a file, useful when drag-dropping a file into the terminal
function fcd () { [ -f "${1}" ] && { cd "$(dirname "${1}")"; } || { cd "${1}"; } ; pwd; }

# create a new directory and enter it - mkdir + cd
function mkd () {
  mkdir -p "$@" && cd "$_";
}

# rename files to underscore-only no accent
function fformat () {
  for f in *; do pre="${f%.*}"; suf="${f##*.}"; mv "$f" $(echo $pre | iconv -f utf8 -t ascii//TRANSLIT | sed -E 's/[^[:alnum:]]+/_/g' <<<"$s").${suf}; done
  # example : "MRC1  _ l'étude çours-f.25122019.pptx" >>> "MRC1_l_etude_cours_f_25122019.pptx"
}

# wrapper for easy extraction of compressed files
function extract () {
  if [ -f ${1} ] ; then
      case ${1} in
          *.tar.xz)    tar xvJf ${1}    ;;
          *.tar.bz2)   tar xvjf ${1}    ;;
          *.tar.gz)    tar xvzf ${1}    ;;
          *.bz2)       bunzip2 ${1}     ;;
          *.rar)       unrar e ${1}     ;;
          *.gz)        gunzip ${1}      ;;
          *.tar)       tar xvf ${1}     ;;
          *.tbz2)      tar xvjf ${1}    ;;
          *.tgz)       tar xvzf ${1}    ;;
          *.apk)       unzip ${1}       ;;
          *.epub)      unzip ${1}       ;;
          *.xpi)       unzip ${1}       ;;
          *.zip)       unzip ${1}       ;;
          *.war)       unzip ${1}       ;;
          *.jar)       unzip ${1}       ;;
          *.Z)         uncompress ${1}  ;;
          *.7z)        7z x ${1}        ;;
          *)           echo "don't know how to extract '${1}'..." ;;
      esac
  else
      echo "'${1}' is not a valid file!"
  fi
}

# create a random folder in /tmp and cd into it
function tmp {
  cd $(mktemp -d /tmp/${1}_XXXX)
}

# johnnydecimal cd - johnnydecimal.com/concepts/working-at-the-terminal/
function cjd () {
  pushd ~/Nextcloud/Documents/*/*/${1}*
}

function youtube-mp3 () {
  youtube-dl --ignore-errors -f bestaudio --extract-audio --audio-format mp3 --audio-quality 0 -o '~/Music/%(title)s.%(ext)s' "${1}"
}

function youtube-mp4() {
  youtube-dl --write-auto-sub -no-playlist -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/mp4' -o '~/Movies/%(title)s.%(ext)s' "${1}"
}

function pdfcompress() {
  # source : https://gist.github.com/ahmed-musallam/27de7d7c5ac68ecbd1ed65b6b48416f9
  # brew install ghostscript
  gs -q -dNOPAUSE -dBATCH -dSAFER -sDEVICE=pdfwrite -dCompatibilityLevel=1.3 -dPDFSETTINGS=/screen -dEmbedAllFonts=true -dSubsetFonts=true -dColorImageDownsampleType=/Bicubic -dColorImageResolution=144 -dGrayImageDownsampleType=/Bicubic -dGrayImageResolution=144 -dMonoImageDownsampleType=/Bicubic -dMonoImageResolution=144 -sOutputFile=compressed.${1} ${1};
}

function update() {
  echo "updating brew ..."
  brew update
  echo "upgrading brew ..."
  brew upgrade
  brew cask upgrade
  echo "cleaning brew ..."
  brew cleanup -s
  #now diagnotic
  brew doctor
  brew missing

  echo "updating apps from the mac app store ..."
  if brew ls --versions mas > /dev/null; then
    mas upgrade
  else
    brew install mas
    mas upgrade
  fi
  echo "updating npm ..."
  npm update -g
  echo "updating gem ..."
  gem update
  echo "updating pip and pip apps ..."
  pip3 install --upgrade pip
  if test ! $(which pip-chill); then
    pip3 install pip-chill
  fi
  pip-chill --no-version | xargs pip3 install -U
}

function mdexe() {
  if [ -f "${1}" ]; then
    cat ${1} | # print the file
    sed -n '/```bash/,/```/p' | # get the bash code blocks
    sed 's/```bash//g' | #  remove the ```bash
    sed 's/```//g' | # remove the trailing ```
    sed '/^$/d' | # remove empty lines
    ${SHELL} ; # execute the command
  else
    echo "${1} is not valid" ;
  fi
}

function clip() {
  cat $1 | pbcopy
}

# find where a text is located in current dir
function findtext() {
  grep -rnw . -e "$*"
}
